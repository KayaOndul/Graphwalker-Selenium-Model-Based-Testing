

## Testing Goodreads.com with Graphwalker

There are 3 models:

- Add book to your wishlist and remove
- Change rating a book in your wishlist
- Search for a member to add as a friend

To run it:
- ```mvn clean```
- ```mvn graphwalker:generate-sources```
- ```mvn graphwalker:test```

Reports will generated automatically.
