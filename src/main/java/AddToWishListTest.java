import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.java.annotation.AfterExecution;
import org.graphwalker.java.annotation.BeforeExecution;
import org.graphwalker.java.annotation.GraphWalker;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@GraphWalker(value = "random(edge_coverage(100)) ", start = "e_StartBrowser")
public class AddToWishListTest extends ExecutionContext implements AddToYourWishList {


    private static final Logger logger = LoggerFactory.getLogger(AddToYourWishList.class);
    WebDriver driver = null;
    WebDriverWait waiter = null;


    @BeforeExecution
    public void setup() {
        FirefoxDriverManager.getInstance().setup();
    }

    @AfterExecution
    public void cleanup() {
        if (driver != null) {
            driver.quit();
        }
        driver.quit();

    }

    public void e_myBooks() {
        logger.debug("IN E_MYBOOKS");
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("My Books")));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.linkText("My Books")).click();
    }


    public void v_MyBooks() {
        logger.debug("IN V_MYBOOKS");
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Yeni Hayat")));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void e_AddToWishList() {
        logger.debug("IN e_AddToWishList");

        driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[1]/div[2]/div[2]/table/tbody/tr[1]/td[2]/div[2]/div/div[1]/form/button")).click();


    }

    public void e_removeFromWishlList() {
        logger.debug("IN e_removeFromWishlList");
        driver.findElement(By.cssSelector("a[href*='destroy/668194']")).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        waiter.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        alert.accept();
        setAttribute("addToWish", false);
    }


    public void e_StartBrowser() {
        logger.debug("IN e_StartBrowser");

        driver = new FirefoxDriver();
        Assert.assertNotNull(driver);
        waiter = new WebDriverWait(driver, 10);
    }

    public void v_SearchResult() {
        logger.debug("IN v_searchresult");


        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.id("1_book_668194")));
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div[3]/div[1]/div[2]/div[2]/table/tbody/tr[1]/td[2]/div[2]/div/div[1]/form/button")));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void e_EnterBaseURL() {
        driver.get("http://www.goodreads.com");
        waiter.until(ExpectedConditions.titleContains("Goodreads"));
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"userSignInFormEmail\"]")));
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"user_password\"]")));

        driver.findElement(By.xpath("//*[@id=\"userSignInFormEmail\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"userSignInFormEmail\"]")).sendKeys("swe550@yopmail.com");
        driver.findElement(By.xpath("//*[@id=\"user_password\"]")).sendKeys("123456");
        driver.findElement(By.xpath("//*[@id=\"sign_in\"]/div[3]/input[1]")).click();


    }

    public void v_BaseURL() {
        waiter.until(ExpectedConditions.titleContains("Goodreads"));
    }

    public void v_BrowserStarted() {
        Assert.assertNotNull(driver);
    }

    public void e_SearchBook() {

        logger.debug("IN e_searchbook");
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Search books']")));
        driver.findElement(By.xpath("//input[@placeholder='Search books']")).clear();
        driver.findElement(By.xpath("//input[@placeholder='Search books']")).sendKeys("Yeni Hayat" + Keys.ENTER);
    }
}

